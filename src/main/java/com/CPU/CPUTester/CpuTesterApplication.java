package com.CPU.CPUTester;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CpuTesterApplication {

	public static void main(String[] args) {
		SpringApplication.run(CpuTesterApplication.class, args);
	}

}
