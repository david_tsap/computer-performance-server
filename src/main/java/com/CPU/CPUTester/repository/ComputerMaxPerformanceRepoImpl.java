package com.CPU.CPUTester.repository;

import com.CPU.CPUTester.servicemodel.MaxPerformance;
import org.springframework.stereotype.Service;

@Service
public class ComputerMaxPerformanceRepoImpl implements ComputerMaxPerformanceRepo{

  MaxPerformance maxPerformance = new MaxPerformance();

  @Override
  public MaxPerformance getMaxPerformance() {
    return maxPerformance;
  }

  @Override
  public void setMaxPerformance(MaxPerformance maxPerformance) {
    this.maxPerformance = maxPerformance;
  }
}
