package com.CPU.CPUTester.repository;

import com.CPU.CPUTester.servicemodel.MaxPerformance;

public interface ComputerMaxPerformanceRepo {

  MaxPerformance getMaxPerformance();
  void setMaxPerformance(MaxPerformance maxPerformance);
}
