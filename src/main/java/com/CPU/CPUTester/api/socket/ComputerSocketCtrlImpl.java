package com.CPU.CPUTester.api.socket;

import com.CPU.CPUTester.service.ComputerMaxPerformanceService;
import com.CPU.CPUTester.service.ComputerPerformance;
import com.CPU.CPUTester.viewmodel.Computer;
import com.CPU.CPUTester.servicemodel.MaxPerformance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class ComputerSocketCtrlImpl{

  private static final Logger log = LoggerFactory.getLogger(ComputerSocketCtrlImpl.class);

  @Autowired
  private ComputerMaxPerformanceService computerMaxPerformanceService;

  @Autowired
  private ComputerPerformance computerPerformance;

  @MessageMapping("/setMax")
  @SendTo("/topic/performance")
  public Computer performance(MaxPerformance maxPerformance) throws Exception {
    log.info("get Max CPU and RAM, CPU: " + maxPerformance.getMaxCpu() +" RAM: " + maxPerformance.getMaxRam());
    computerMaxPerformanceService.save(maxPerformance);
    return computerPerformance.getComputerPerformance();
  }
}
