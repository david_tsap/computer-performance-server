package com.CPU.CPUTester.service;

import com.CPU.CPUTester.repository.ComputerMaxPerformanceRepoImpl;
import com.CPU.CPUTester.servicemodel.MaxPerformance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ComputerMaxPerformanceServiceImpl implements ComputerMaxPerformanceService{

  @Autowired
  ComputerMaxPerformanceRepoImpl computerMaxPerformanceRepo;

  @Autowired
  RamGenerator ramGenerator;

  @Override
  public MaxPerformance getMaxPerformance() {
    return computerMaxPerformanceRepo.getMaxPerformance();
  }

  @Override
  public void save(MaxPerformance maxPerformance) {
    computerMaxPerformanceRepo.setMaxPerformance(maxPerformance);
    ramGenerator.increaseRam(maxPerformance.getMaxRam());
  }
}
