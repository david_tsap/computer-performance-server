package com.CPU.CPUTester.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/***
 * by allocation of array in MB(size) we will generate memory
 */
@Service
public class RamGeneratorImpl implements RamGenerator{

  @Autowired
  ComputerMaxPerformanceService computerMaxPerformanceService;

  private static final Logger log = LoggerFactory.getLogger(RamGeneratorImpl.class);
  int oneMegabyte = 1000 * 1000;
  byte[] bytes; // 1MB


  @Override
  public void increaseRam(String maxRam){
    int maxRamInt = Integer.parseInt(maxRam);
    log.info("creating Ram thread");
    bytes = new byte[oneMegabyte * maxRamInt];
  }
}
