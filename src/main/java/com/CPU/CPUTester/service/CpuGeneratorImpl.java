package com.CPU.CPUTester.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/***
 * By increasing the number of thread that always open
 * we will increase the CPU load
 */
@Service
public class CpuGeneratorImpl implements CpuGenerator{

  @Autowired
  ComputerMaxPerformanceService computerMaxPerformanceService;

  @Scheduled(fixedRate = 100)
  @Override
  public void increaseCPU(){
    String maxCpu = computerMaxPerformanceService.getMaxPerformance().getMaxCpu();
    for (int i = 0; i < 10* Integer.parseInt(maxCpu) ; i++) {
      Thread thread =
          new Thread() {
            public void run() {
              try {
                Thread.sleep(50);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
          };
      thread.start();
    }
  }
}
