package com.CPU.CPUTester.service;

import com.CPU.CPUTester.viewmodel.Computer;

public interface ComputerPerformance {

  public Computer getComputerPerformance() throws Exception;
}
