package com.CPU.CPUTester.service;

import com.CPU.CPUTester.viewmodel.Computer;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import org.springframework.stereotype.Service;

@Service
public class ComputerPerformanceImpl implements ComputerPerformance{

  @Override
  public Computer getComputerPerformance() throws Exception {
    Computer computer = new Computer();
    double systemLoadAverage = getProcessCpuLoad();
    int ramUsage = getRamUsage();
    computer.setCpu(String.valueOf(systemLoadAverage));
    computer.setRam(String.valueOf(ramUsage));
    return computer;
  }

  private int getRamUsage() {
    MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
    MemoryUsage heapMemoryUsage = memoryMXBean.getHeapMemoryUsage();
    double ramUsage = (double) heapMemoryUsage.getUsed() / heapMemoryUsage.getCommitted() * 100;
    return (int)ramUsage;
  }

  public double getProcessCpuLoad() throws Exception {
    MBeanServer mbs    = ManagementFactory.getPlatformMBeanServer();
    ObjectName name    = ObjectName.getInstance("java.lang:type=OperatingSystem");
    AttributeList list = mbs.getAttributes(name, new String[]{ "SystemCpuLoad" });

    if (list.isEmpty())     return Double.NaN;

    Attribute att = (Attribute)list.get(0);
    Double value  = (Double)att.getValue();

    // usually takes a couple of seconds before we get real values
    if (value == -1.0)      return Double.NaN;
    // returns a percentage value with 1 decimal point precision
    return ((int)(value * 1000) / 10.0);
  }

}
