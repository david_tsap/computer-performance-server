package com.CPU.CPUTester.service;

import com.CPU.CPUTester.servicemodel.MaxPerformance;

public interface ComputerMaxPerformanceService{

   void save(MaxPerformance maxPerformance);
   MaxPerformance getMaxPerformance();
}
