package com.CPU.CPUTester.servicemodel;



public class MaxPerformance {

  private String maxCpu = "1";
  private String maxRam = "1";

  public String getMaxCpu() {
    return maxCpu;
  }

  public void setMaxCpu(String maxCpu) {
    this.maxCpu = maxCpu;
  }

  public String getMaxRam() {
    return maxRam;
  }

  public void setMaxRam(String maxRam) {
    this.maxRam = maxRam;
  }
}
