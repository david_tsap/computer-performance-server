package com.CPU.CPUTester.scheduler;

import com.CPU.CPUTester.service.ComputerPerformance;
import com.CPU.CPUTester.viewmodel.Computer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/***
 * writing computer data to topic every half second.
 */
@Component
public class ReportPerformanceScheduledTasks implements ReportPerformance {

  private static final Logger log = LoggerFactory.getLogger(ReportPerformanceScheduledTasks.class);

  @Autowired
  private SimpMessagingTemplate template;

  @Autowired
  ComputerPerformance computerPerformance;

  @Scheduled(fixedRate = 500)
  public void reportPerformance() throws Exception {
    Computer computer = computerPerformance.getComputerPerformance();
    log.info("computer: CPU: " + computer.getCpu() + " RAM: " + computer.getRam() );
    template.convertAndSend("/topic/performance",computer);
  }

}
